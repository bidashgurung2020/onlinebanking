package GUI;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import Implementation.AccountInvoker;
import Implementation.LoginImpl;

public class UpdatePinGUI extends JFrame {
    private JPasswordField mPinField;
    private JPasswordField tPinField;
    private String option = "pin";
    private LoginImpl login;

    public UpdatePinGUI() {
        setTitle("Update PIN");
        setSize(300, 200);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        login = LoginImpl.getInstance();

        // Create input fields
        mPinField = new JPasswordField(20);
        tPinField = new JPasswordField(20);

        // Create labels
        JLabel mPinLabel = new JLabel("New mPIN:");
        JLabel tPinLabel = new JLabel("New tPIN:");

        // Create update button
        JButton updateButton = new JButton("Update");

        // Add action listener to the update button
        updateButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int userId = login.getUserId(); // Replace with actual user ID
                String m = new String(mPinField.getPassword());
                String t = new String(tPinField.getPassword());
                
                int mPin = Integer.parseInt(m);
                int tPin = Integer.parseInt(t);
                
                // Implementing AccountInvoker Implementation
                AccountInvoker accInv = new AccountInvoker();
                accInv.userId = userId;
            	accInv.mPin=mPin;
            	accInv.tPin = tPin;
            	accInv.setCommand(option);
            	
            	if (m.isEmpty() || t.isEmpty()) {
                    JOptionPane.showMessageDialog(UpdatePinGUI.this, "Please fill in all fields.", "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }


                int response = JOptionPane.showConfirmDialog(
                        UpdatePinGUI.this,
                        "Are you sure you want to update your PINs?",
                        "Confirm Update",
                        JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE
                );

                if (response == JOptionPane.YES_OPTION) {
                    accInv.executeCommand();
                    JOptionPane.showMessageDialog(UpdatePinGUI.this, "Update successful.");
                } else {
                    accInv.undoCommand();
                }
            }
        });

        // Create panel for input fields and labels
        JPanel inputPanel = new JPanel(new GridLayout(3, 2));
        inputPanel.add(mPinLabel);
        inputPanel.add(mPinField);
        inputPanel.add(tPinLabel);
        inputPanel.add(tPinField);

        // Add components to frame
        getContentPane().add(inputPanel, BorderLayout.CENTER);
        getContentPane().add(updateButton, BorderLayout.SOUTH);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
            	LoginImpl loginImpl = LoginImpl.getInstance();
                // Check if the user is logged in
                int userId = loginImpl.getUserId();
                if (loginImpl.isLoggedIn(userId)) {
                    UpdatePinGUI pinPageGUI = new UpdatePinGUI();
                    pinPageGUI.setVisible(true);
                } else {
                    // Show login GUI or perform other actions as needed
                    LoginGUI loginGUI = new LoginGUI();
                    loginGUI.setVisible(true);
                }
            }
        });
    }
}
