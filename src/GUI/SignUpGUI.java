package GUI;

import javax.swing.*;

import Implementation.SignUpProxyImpl;
import com.OnlineBanking.auth.*;
import com.OnlineBanking.util.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SignUpGUI extends JFrame {
    private JTextField usernameField;
    private JTextField cidField;
    private JTextField emailField;
    private JTextField accNumField;
    private JPasswordField tpinField;
    private JPasswordField mpinField;
    private SignUpProxyImpl signUpProxy;

    public SignUpGUI() {
        setTitle("User Sign Up");
        setSize(400, 350); // Increased height to accommodate login link
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        
        signUpProxy = new SignUpProxyImpl();

        // Create labels and text fields
        JLabel usernameLabel = new JLabel("Username:");
        usernameField = new JTextField(20);
        
        JLabel cidLabel = new JLabel("CID");
        cidField = new JTextField(11);

        JLabel emailLabel = new JLabel("Email:");
        emailField = new JTextField(20);

        JLabel accNumLabel = new JLabel("Account Number:");
        accNumField = new JTextField(10);
        
        JLabel mpinLabel = new JLabel("M-PIN:");
        mpinField = new JPasswordField(4);

        JLabel tpinLabel = new JLabel("T-PIN:");
        tpinField = new JPasswordField(4);

        // Create sign up button
        JButton signUpButton = new JButton("Sign Up");
        signUpButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                signUp();
            }
        });

        // Create login link label
        JLabel loginLinkLabel = new JLabel("Already have an account? Login");
        loginLinkLabel.setForeground(Color.BLUE); // Set text color to blue for a clickable link
        loginLinkLabel.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR)); // Change cursor to hand when hovering
        loginLinkLabel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                openLoginGUI();
            }
        });

        // Create nested panels with margins
        JPanel usernamePanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 10, 5));
        usernamePanel.add(usernameLabel);
        usernamePanel.add(usernameField);
        
        JPanel cidPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 10, 5));
        cidPanel.add(cidLabel);
        cidPanel.add(cidField);

        JPanel emailPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 10, 5));
        emailPanel.add(emailLabel);
        emailPanel.add(emailField);

        JPanel accNumPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 10, 5));
        accNumPanel.add(accNumLabel);
        accNumPanel.add(accNumField);
        
        JPanel mpinPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 10, 5));
        mpinPanel.add(mpinLabel);
        mpinPanel.add(mpinField);

        JPanel tpinPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 10, 5));
        tpinPanel.add(tpinLabel);
        tpinPanel.add(tpinField);

        // Create panel for input fields
        JPanel inputPanel = new JPanel();
        inputPanel.setLayout(new BoxLayout(inputPanel, BoxLayout.Y_AXIS));
        inputPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10)); // Add margin around the input panel
        inputPanel.add(usernamePanel);
        inputPanel.add(cidPanel);
        inputPanel.add(emailPanel);
        inputPanel.add(accNumPanel);
        inputPanel.add(mpinPanel);
        inputPanel.add(tpinPanel);

        // Create panel for button and login link
        JPanel buttonPanel = new JPanel(new GridLayout(2, 1, 0, 5));
        buttonPanel.add(signUpButton);
        buttonPanel.add(loginLinkLabel);

        // Add input panel and button panel to main panel
        JPanel mainPanel = new JPanel(new BorderLayout());
        mainPanel.add(inputPanel, BorderLayout.CENTER);
        mainPanel.add(buttonPanel, BorderLayout.SOUTH);

        // Add main panel to frame
        getContentPane().add(mainPanel);
    }

    private void signUp() {
        // Retrieve input values from text fields
        String userName = usernameField.getText().trim();
        String cId = cidField.getText().trim();
        String email = emailField.getText().trim();
        String accNo = accNumField.getText().trim();
        String mPin = new String(mpinField.getPassword()).trim();
        String tPin = new String(tpinField.getPassword()).trim();
        
   
        if (userName.isEmpty() || cId.isEmpty()|| email.isEmpty() || accNo.isEmpty() || tPin.isEmpty() || mPin.isEmpty()) {
            JOptionPane.showMessageDialog(this, "Please fill in all fields.", "Error", JOptionPane.ERROR_MESSAGE);
            return;
        }

        try {
        	 int parsedMPin = Integer.parseInt(mPin);
             int parsedTPin = Integer.parseInt(tPin);
        	
        	User user = new User(userName, cId, parsedMPin, parsedTPin, accNo, email);
        	
        	// Invoke signUp method of SignUpProxyImpl
            signUpProxy.signUp(user);
            
            usernameField.setText("");
            cidField.setText("");
            emailField.setText("");
            accNumField.setText("");
            mpinField.setText("");
            tpinField.setText("");
            
//            JOptionPane.showMessageDialog(this, "User signed up successfully.", "Success", JOptionPane.INFORMATION_MESSAGE);
        } catch (NumberFormatException ex) {
            JOptionPane.showMessageDialog(this, "Invalid input format.", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void openLoginGUI() {
        LoginGUI loginGUI = new LoginGUI();
        loginGUI.setVisible(true);
        dispose();
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new SignUpGUI().setVisible(true);
            }
        });
    }
}
