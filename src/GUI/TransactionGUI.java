package GUI;

import javax.swing.*;

import Implementation.LoginImpl;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class TransactionGUI extends JFrame {
    public TransactionGUI() {
        setTitle("Transaction");
        setSize(300, 200);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);

        // Create buttons
        JButton rechargeButton = new JButton("Recharge");
        JButton transferButton = new JButton("Transfer");

        // Set preferred size for buttons
        rechargeButton.setPreferredSize(new Dimension(90, 20)); // Width: 120, Height: 30
        transferButton.setPreferredSize(new Dimension(90, 20)); // Width: 120, Height: 30

        // Add action listeners to buttons
        rechargeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                openRechargeGUI();
                dispose();
            }
        });

        transferButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                openTransferGUI();
                dispose();
            }
        });

        // Create panel for buttons
        JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 10, 5));
        buttonPanel.add(rechargeButton);
        buttonPanel.add(transferButton);

        // Add button panel to main panel
        JPanel mainPanel = new JPanel(new BorderLayout());
        mainPanel.add(buttonPanel, BorderLayout.CENTER);

        // Add main panel to frame
        getContentPane().add(mainPanel);
    }

    private void openRechargeGUI() {
        RechargeGUI rechargeGUI = new RechargeGUI();
        rechargeGUI.setVisible(true);
    }

    private void openTransferGUI() {
        TransferGUI transferGUI = new TransferGUI();
        transferGUI.setVisible(true);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
            	LoginImpl loginImpl = LoginImpl.getInstance();
                // Check if the user is logged in
                int userId = loginImpl.getUserId();
                if (loginImpl.isLoggedIn(userId)) {
                    MainPageGUI mainPageGUI = new MainPageGUI();
                    mainPageGUI.setVisible(true);
                } else {
                    // Show login GUI or perform other actions as needed
                    LoginGUI loginGUI = new LoginGUI();
                    loginGUI.setVisible(true);
                }
            }
        });
    }
}
