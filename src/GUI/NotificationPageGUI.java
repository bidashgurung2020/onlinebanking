package GUI;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import Implementation.AdminMessaging;
import Implementation.LoginImpl;
import javax.swing.table.DefaultTableModel;

public class NotificationPageGUI extends JFrame {
    private JTable table;
    private DefaultTableModel tableModel;
    private Timer timer;
    private String option="receive";
    private AdminMessaging message;

    public NotificationPageGUI() {
        setTitle("Notification");
        setSize(600, 400);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());
        message = new AdminMessaging();

        // Table setup
        tableModel = new DefaultTableModel(new String[]{"Message", "Received At"}, 0);
        table = new JTable(tableModel);
        add(new JScrollPane(table), BorderLayout.CENTER);

        // Initialize user observer with the table model
        message.tableModel=tableModel;

        // Timer to refresh table data
        timer = new Timer(10000, e -> fetchMessages());
        timer.start();

        // Initial fetch of data
        fetchMessages();
    }

    private void fetchMessages() {
        message.executeAdminMessaging(option);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
        	LoginImpl loginImpl = LoginImpl.getInstance();
            // Check if the user is logged in
            int userId = loginImpl.getUserId();
            if (loginImpl.isLoggedIn(userId)) {
                NotificationPageGUI notifyPageGUI = new NotificationPageGUI();
                notifyPageGUI.setVisible(true);
            } else {
                // Show login GUI or perform other actions as needed
                LoginGUI loginGUI = new LoginGUI();
                loginGUI.setVisible(true);
            }
        });
    }
}

