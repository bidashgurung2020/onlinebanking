package GUI;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import Implementation.AdminMessaging;
import Implementation.LoginImpl;

public class DashboardPageGUI extends JFrame {
    private JTextArea messageTextArea;
    private String option = "send";
    private LoginImpl loginImpl;
    

    public DashboardPageGUI() {
        setTitle("Dashboard");
        setSize(400, 300);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        loginImpl = LoginImpl.getInstance();

        // Create labels and text area
        JLabel messageLabel = new JLabel("Write your message:");
        messageTextArea = new JTextArea(10, 30);
        JScrollPane scrollPane = new JScrollPane(messageTextArea);

        // Create submit button
        JButton submitButton = new JButton("Submit");
        submitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                submitMessage();
            }
        });
        
        JButton logoutButton = new JButton("Logout");
        logoutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                logout();
            }
        });

        // Create panel for form elements
        JPanel formPanel = new JPanel();
        formPanel.setLayout(new BoxLayout(formPanel, BoxLayout.Y_AXIS));
        formPanel.add(messageLabel);
        formPanel.add(scrollPane);
        formPanel.add(Box.createVerticalStrut(10)); // Add vertical space between elements
        formPanel.add(submitButton);
        formPanel.add(Box.createVerticalStrut(5)); // Add more vertical space between buttons
        formPanel.add(logoutButton);

        // Add form panel to the frame
        getContentPane().add(formPanel, BorderLayout.CENTER);
    }

    private void submitMessage() {
        // Get the message from the text area
        String message = messageTextArea.getText();
        AdminMessaging sendMessage = new AdminMessaging();
        sendMessage.message= message;
        
        sendMessage.executeAdminMessaging(option);
        JOptionPane.showMessageDialog(this, "Notification Sent Successfully!!!", "Notification", JOptionPane.INFORMATION_MESSAGE);

        // Clear the text area after submission
        messageTextArea.setText("");
    }
    
    private void logout() {
        int userId = loginImpl.getUserId();
        System.out.println(userId);
        loginImpl.logout(userId);
        JOptionPane.showMessageDialog(this, "Logout successful.", "Success", JOptionPane.INFORMATION_MESSAGE);
        LoginGUI loginGUI = new LoginGUI();
        loginGUI.setVisible(true);
        dispose();
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
            	LoginImpl loginImpl = LoginImpl.getInstance();
                // Check if the user is logged in
                int userId = loginImpl.getUserId();
                if (loginImpl.isLoggedIn(userId)) {
                    DashboardPageGUI dashPageGUI = new DashboardPageGUI();
                    dashPageGUI.setVisible(true);
                } else {
                    // Show login GUI or perform other actions as needed
                    LoginGUI loginGUI = new LoginGUI();
                    loginGUI.setVisible(true);
                }
            }
        });
    }
}

