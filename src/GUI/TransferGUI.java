package GUI;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import Implementation.TransactionImpl;
import Implementation.LoginImpl;

//Database 
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.ResultSet;

public class TransferGUI extends JFrame {
    private JTextField amountField;
    private JTextField fromAccountField;
    private JTextField toAccountField;
    private JButton transferButton;
    private TransactionImpl transactionImpl;
    private Connection connection;
    private LoginImpl loginImpl;

    public TransferGUI() {
        setTitle("Transfer");
        setSize(300, 200);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setLocationRelativeTo(null);
        loginImpl= loginImpl.getInstance();
        transactionImpl= new TransactionImpl();
        connectToDatabase();
        
        int id = loginImpl.getUserId();
        String accNo = getUserAccountFromDatabase(id);

        // Initialize components
        amountField = new JTextField(10);
        fromAccountField = new JTextField(accNo, 10);
        fromAccountField.setEditable(false);
        toAccountField = new JTextField(10);
        transferButton = new JButton("Transfer");

        // Add action listener to transfer button
        transferButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                performTransfer();
            }
        });

        // Create panel for components
        JPanel panel = new JPanel(new GridLayout(4, 1, 10, 10));
        panel.add(new JLabel("Amount to Transfer:"));
        panel.add(amountField);
        panel.add(new JLabel("From Account:"));
        panel.add(fromAccountField);
        panel.add(new JLabel("To Account:"));
        panel.add(toAccountField);
        panel.add(transferButton);

        // Add panel to frame
        getContentPane().add(panel, BorderLayout.CENTER);
    }
     
    private void connectToDatabase() {
        try {
            // Connect to the database (replace the connection parameters with your actual database details)
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/pattern_design", "root", "");
        } catch (SQLException e) {
            System.err.println("Error connecting to the database: " + e.getMessage());
            JOptionPane.showMessageDialog(null, "Error connecting to the database", "Database Error", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    private void performTransfer() {
        int id = loginImpl.getUserId();
        double balance = getUserBalanceFromDatabase(id);

        String amountStr = amountField.getText();
        String fromAccount = fromAccountField.getText();
        String toAccount = toAccountField.getText();

        double amount = Double.parseDouble(amountStr);

        if (amountStr.isEmpty() || fromAccount.isEmpty() || toAccount.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Please fill in all fields.", "Error", JOptionPane.ERROR_MESSAGE);
            return;
        }

        // Prompt the user to enter TPIN
        String enteredTPIN = JOptionPane.showInputDialog(null, "Enter TPIN:", "TPIN Required", JOptionPane.PLAIN_MESSAGE);

        // Retrieve user's TPIN from database
        String userTPIN = getUserTPINFromDatabase(id);

        // Validate entered TPIN
        if (enteredTPIN == null || !enteredTPIN.equals(userTPIN)) {
            JOptionPane.showMessageDialog(null, "Incorrect TPIN. Transaction aborted.", "Error", JOptionPane.ERROR_MESSAGE);
            return;
        }

        // If TPIN is correct, proceed with the transaction
        transactionImpl.performTransfer(amount, fromAccount, toAccount, balance);
        dispose();
    }
    
    private String getUserAccountFromDatabase(int userId) {
	    String account = null;
	    try {
	        // Assuming you have a Connection object named 'connection' established
	        String query = "SELECT accNo FROM users WHERE id = ?";
	        try (PreparedStatement statement = connection.prepareStatement(query)) {
	            statement.setInt(1, userId);
	            try (ResultSet resultSet = statement.executeQuery()) {
	                if (resultSet.next()) {
	                    // Retrieve the account information from the result set
	                    account = resultSet.getString("accNo");
	                }
	            }
	        }
	    } catch (SQLException e) {
	        JOptionPane.showMessageDialog(null, "Error accessing database: " + e.getMessage(), "Database Error", JOptionPane.ERROR_MESSAGE);
	    }
	    return account;
	}
    
    // Get Logged In user balance
    private double getUserBalanceFromDatabase(int userId) {
        double balance = 0.0;
        try {
            // Assuming you have a Connection object named 'connection' established
            String query = "SELECT balance FROM users WHERE id = ?";
            try (PreparedStatement statement = connection.prepareStatement(query)) {
                statement.setInt(1, userId);
                try (ResultSet resultSet = statement.executeQuery()) {
                    if (resultSet.next()) {
                        // Retrieve the balance information from the result set
                        balance = resultSet.getDouble("balance");
                    }
                }
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Error accessing database: " + e.getMessage(), "Database Error", JOptionPane.ERROR_MESSAGE);
        }
        return balance;}
    
 // Get user's TPIN from the database
    private String getUserTPINFromDatabase(int userId) {
        String tpin = null;
        try {
            // Assuming you have a Connection object named 'connection' established
            String query = "SELECT tpin FROM users WHERE id = ?";
            try (PreparedStatement statement = connection.prepareStatement(query)) {
                statement.setInt(1, userId);
                try (ResultSet resultSet = statement.executeQuery()) {
                    if (resultSet.next()) {
                        // Retrieve the TPIN information from the result set
                        tpin = resultSet.getString("tpin");
                    }
                }
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Error accessing database: " + e.getMessage(), "Database Error", JOptionPane.ERROR_MESSAGE);
        }
        return tpin;
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
            	LoginImpl loginImpl = LoginImpl.getInstance();
                // Check if the user is logged in
                int userId = loginImpl.getUserId();
                if (loginImpl.isLoggedIn(userId)) {
                    TransferGUI transferPageGUI = new TransferGUI();
                    transferPageGUI.setVisible(true);
                } else {
                    // Show login GUI or perform other actions as needed
                    LoginGUI loginGUI = new LoginGUI();
                    loginGUI.setVisible(true);
                }
            }
        });
    }
}
