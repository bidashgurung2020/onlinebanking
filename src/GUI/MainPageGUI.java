package GUI;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import Implementation.LoginImpl;

public class MainPageGUI extends JFrame {
	private LoginImpl loginImpl;
    public MainPageGUI() {
        setTitle("Main Page");
        setSize(300, 200);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        loginImpl = LoginImpl.getInstance();

        // Create buttons
        JButton transactionButton = new JButton("Transaction");
        JButton updateButton = new JButton("Update");
        JButton notificationButton = new JButton("Notification");
        JButton logoutButton = new JButton("Logout");

        // Add action listeners to buttons
        transactionButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // Redirect to TransactionGUI
                TransactionGUI transactionGUI = new TransactionGUI();
                transactionGUI.setVisible(true);
            }
        });

        updateButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                UpdatePageGUI updateGUI = new UpdatePageGUI();
                updateGUI.setVisible(true);
            }
        });

        notificationButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                NotificationPageGUI notify = new NotificationPageGUI();
                notify.setVisible(true);
            }
        });
        
        logoutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                logout();
            }
        });

        // Create panel for buttons
        JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 10, 5));
        
        buttonPanel.add(transactionButton);
        buttonPanel.add(updateButton);
        buttonPanel.add(notificationButton);
        buttonPanel.add(logoutButton);

        // Add button panel to main panel
        JPanel mainPanel = new JPanel(new BorderLayout());
        mainPanel.add(buttonPanel, BorderLayout.CENTER);

        // Add main panel to frame
        getContentPane().add(mainPanel);
    }
    
    private void logout() {
        int userId = loginImpl.getUserId();
        System.out.println(userId);
        loginImpl.logout(userId);
        JOptionPane.showMessageDialog(this, "Logout successful.", "Success", JOptionPane.INFORMATION_MESSAGE);
        LoginGUI loginGUI = new LoginGUI();
        loginGUI.setVisible(true);
        dispose();
    }

    public static void main(String[] args) {
    	SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                LoginImpl loginImpl = LoginImpl.getInstance();
                // Check if the user is logged in
                int userId = loginImpl.getUserId();
                if (loginImpl.isLoggedIn(userId)) {
                    MainPageGUI mainPageGUI = new MainPageGUI();
                    mainPageGUI.setVisible(true);
                } else {
                    // Show login GUI or perform other actions as needed
                    LoginGUI loginGUI = new LoginGUI();
                    loginGUI.setVisible(true);
                }
            }
        });
    }
}

