package GUI;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import Implementation.TransactionImpl;
import Implementation.LoginImpl;

//Database 
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.ResultSet;

public class RechargeGUI extends JFrame {
    private JTextField amountField;
    private JTextField fromAccountField;
    private JTextField phoneNumField;
    private JButton rechargeButton;
    private TransactionImpl transactionImpl;
    private Connection connection;
    private LoginImpl loginImpl;

    public RechargeGUI() {
        setTitle("Recharge");
        setSize(300, 200);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setLocationRelativeTo(null);
        loginImpl= loginImpl.getInstance();
        transactionImpl= new TransactionImpl();
        connectToDatabase();
        
        int id = loginImpl.getUserId();
        String accNo = getUserAccountFromDatabase(id);
        

        // Initialize components
        amountField = new JTextField(10);
        fromAccountField = new JTextField(accNo, 10);
        fromAccountField.setEditable(false);
        phoneNumField = new JTextField(8);
        rechargeButton = new JButton("Recharge");

        // Add action listener to recharge button
        rechargeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                performRecharge();
            }
        });

        // Create panel for components
        JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT, 10, 5));
        panel.add(new JLabel("Amount to Recharge:"));
        panel.add(amountField);
        panel.add(new JLabel("From Account:"));
        panel.add(fromAccountField);
        panel.add(new JLabel("Enter Phone Number: "));
        panel.add(phoneNumField);
        panel.add(rechargeButton);

        // Add panel to frame
        getContentPane().add(panel, BorderLayout.CENTER);
    }
    private void connectToDatabase() {
        try {
            // Connect to the database (replace the connection parameters with your actual database details)
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/pattern_design", "root", "");
        } catch (SQLException e) {
            System.err.println("Error connecting to the database: " + e.getMessage());
            JOptionPane.showMessageDialog(null, "Error connecting to the database", "Database Error", JOptionPane.ERROR_MESSAGE);
        }
    }
    private void performRecharge() {
    	int id = loginImpl.getUserId();
    	double balance = getUserBalanceFromDatabase(id);
    	
        // Get values from text fields
        String amountStr = amountField.getText();
        String pNum = phoneNumField.getText();
        String fromAccount = fromAccountField.getText();
        
        double amount = Double.parseDouble(amountStr);
        int num = Integer.parseInt(pNum);
        

        if (amountStr.isEmpty() || pNum.isEmpty() || fromAccount.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Please fill in all fields.", "Error", JOptionPane.ERROR_MESSAGE);
            return;
        }
        
        // Prompt the user to enter TPIN
        String enteredTPIN = JOptionPane.showInputDialog(null, "Enter TPIN:", "TPIN Required", JOptionPane.PLAIN_MESSAGE);

        // Retrieve user's TPIN from database
        String userTPIN = getUserTPINFromDatabase(id);

        // Validate entered TPIN
        if (enteredTPIN == null || !enteredTPIN.equals(userTPIN)) {
            JOptionPane.showMessageDialog(null, "Incorrect TPIN. Recharge aborted.", "Error", JOptionPane.ERROR_MESSAGE);
            return;
        }

        
        transactionImpl.performRecharge(amount, fromAccount, num, balance);
        dispose();
    }
    
    // Get Logged in user AccNum
    private String getUserAccountFromDatabase(int userId) {
	    String account = null;
	    try {
	        // Assuming you have a Connection object named 'connection' established
	        String query = "SELECT accNo FROM users WHERE id = ?";
	        try (PreparedStatement statement = connection.prepareStatement(query)) {
	            statement.setInt(1, userId);
	            try (ResultSet resultSet = statement.executeQuery()) {
	                if (resultSet.next()) {
	                    // Retrieve the account information from the result set
	                    account = resultSet.getString("accNo");
	                }
	            }
	        }
	    } catch (SQLException e) {
	        JOptionPane.showMessageDialog(null, "Error accessing database: " + e.getMessage(), "Database Error", JOptionPane.ERROR_MESSAGE);
	    }
	    return account;
	}
    
    // Get Logged In user balance
    private double getUserBalanceFromDatabase(int userId) {
        double balance = 0.0;
        try {
            // Assuming you have a Connection object named 'connection' established
            String query = "SELECT balance FROM users WHERE id = ?";
            try (PreparedStatement statement = connection.prepareStatement(query)) {
                statement.setInt(1, userId);
                try (ResultSet resultSet = statement.executeQuery()) {
                    if (resultSet.next()) {
                        // Retrieve the balance information from the result set
                        balance = resultSet.getDouble("balance");
                    }
                }
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Error accessing database: " + e.getMessage(), "Database Error", JOptionPane.ERROR_MESSAGE);
        }
        return balance;
    }
   
 // Get user's TPIN from the database
    private String getUserTPINFromDatabase(int userId) {
        String tpin = null;
        try {
            // Assuming you have a Connection object named 'connection' established
            String query = "SELECT tpin FROM users WHERE id = ?";
            try (PreparedStatement statement = connection.prepareStatement(query)) {
                statement.setInt(1, userId);
                try (ResultSet resultSet = statement.executeQuery()) {
                    if (resultSet.next()) {
                        // Retrieve the TPIN information from the result set
                        tpin = resultSet.getString("tpin");
                    }
                }
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Error accessing database: " + e.getMessage(), "Database Error", JOptionPane.ERROR_MESSAGE);
        }
        return tpin;
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
            	LoginImpl loginImpl = LoginImpl.getInstance();
                // Check if the user is logged in
                int userId = loginImpl.getUserId();
                if (loginImpl.isLoggedIn(userId)) {
                    RechargeGUI rechargePageGUI = new RechargeGUI();
                    rechargePageGUI.setVisible(true);
                } else {
                    // Show login GUI or perform other actions as needed
                    LoginGUI loginGUI = new LoginGUI();
                    loginGUI.setVisible(true);
                }
            }
        });
    }
}
