package GUI;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import Implementation.LoginImpl;

public class LoginGUI extends JFrame {
    private JTextField cidField;
    private JPasswordField mpinField;
    private LoginImpl loginImpl;

    public LoginGUI() {
        setTitle("Login");
        setSize(300, 200);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        
        loginImpl = LoginImpl.getInstance();

        // Create labels and text fields
        JLabel cidLabel = new JLabel("CID:");
        cidField = new JTextField(15);

        JLabel mpinLabel = new JLabel("MPIN:");
        mpinField = new JPasswordField(15);

        // Create login button
        JButton loginButton = new JButton("Login");
        loginButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                login();
            }
        });

        // Create sign-up button
        JLabel SignUpLinkLabel = new JLabel("Already have an account? Login");
        SignUpLinkLabel.setForeground(Color.BLUE); // Set text color to blue for a clickable link
        SignUpLinkLabel.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR)); // Change cursor to hand when hovering
        SignUpLinkLabel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                openSignUpGUI();
            }
        });

        // Create panel for input fields
        JPanel inputPanel = new JPanel(new GridLayout(2, 2, 10, 10));
        inputPanel.add(cidLabel);
        inputPanel.add(cidField);
        inputPanel.add(mpinLabel);
        inputPanel.add(mpinField);

        // Create panel for buttons
        JPanel buttonPanel = new JPanel(new GridLayout(2, 1, 0, 10));
        buttonPanel.add(loginButton);
        buttonPanel.add(SignUpLinkLabel);

        // Create main panel
        JPanel mainPanel = new JPanel(new BorderLayout());
        mainPanel.add(inputPanel, BorderLayout.CENTER);
        mainPanel.add(buttonPanel, BorderLayout.SOUTH);

        // Add main panel to frame
        getContentPane().add(mainPanel);
    }

    private void login() {
        // Get input values from text fields
        String cId = cidField.getText();
        String mPin = new String(mpinField.getPassword());
        
        if (cId.isEmpty()|| mPin.isEmpty()) {
            JOptionPane.showMessageDialog(this, "Please fill in all fields.", "Error", JOptionPane.ERROR_MESSAGE);
            return;
        }

        try {
            if (loginImpl.login(cId, Integer.parseInt(mPin))) {
            	String role = loginImpl.getRole();
                if (role != null) {
                    if (role.equals("user")) {
                        // Open main page GUI
                        MainPageGUI mainPageGUI = new MainPageGUI();
                        mainPageGUI.setVisible(true);
                        dispose(); // Close the login GUI
                    } else if (role.equals("admin")) {
                        DashboardPageGUI dashboardPageGUI = new DashboardPageGUI();
                        dashboardPageGUI.setVisible(true);
                        dispose(); // Close the login GUI
                    }
            } else {
                // Login failed, display error message
                JOptionPane.showMessageDialog(this, "Login failed. Invalid CID or MPIN.", "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
         } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(this, "Invalid mpin format. Please enter a valid number.", "Error", JOptionPane.ERROR_MESSAGE);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "An error occurred during login: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void openSignUpGUI() {
        // Code to open the sign-up GUI goes here
        // Replace this with your actual code to open the sign-up GUI
        SignUpGUI signUpGUI = new SignUpGUI();
        signUpGUI.setVisible(true);

        // Close the current login GUI
        dispose();
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new LoginGUI().setVisible(true);
            }
        });
    }
}
