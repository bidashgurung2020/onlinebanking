package GUI;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import Implementation.AccountInvoker;
import Implementation.LoginImpl;

public class UpdateEmailNameGUI extends JFrame {
    private JTextField emailField;
    private JTextField userNameField;
    private String option = "Email&Name";
    private LoginImpl login;

    public UpdateEmailNameGUI() {
        setTitle("Update Email and Name");
        setSize(300, 200);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        login = LoginImpl.getInstance();

        // Create input fields
        userNameField = new JTextField(20);
        emailField = new JTextField(20);
        

        // Create labels
        JLabel userNameLabel = new JLabel("New UserName:");
        JLabel emailLabel = new JLabel("New Email:");

        // Create update button
        JButton updateButton = new JButton("Update");

        // Add action listener to the update button
        updateButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int userId = login.getUserId(); // Replace with actual user ID
                String email = emailField.getText();
                String userName = userNameField.getText();
                AccountInvoker accInv = new AccountInvoker();
                accInv.userId=userId;
                accInv.email=email;
                accInv.userName=userName;
                accInv.setCommand(option);
                
                if (userName.isEmpty() || email.isEmpty()) {
                    JOptionPane.showMessageDialog(UpdateEmailNameGUI.this, "Please fill in all fields.", "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }

                int response = JOptionPane.showConfirmDialog(
                        UpdateEmailNameGUI.this,
                        "Are you sure you want to update your email and username?",
                        "Confirm Update",
                        JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE
                );

                if (response == JOptionPane.YES_OPTION) {
                    accInv.executeCommand();
                    JOptionPane.showMessageDialog(UpdateEmailNameGUI.this, "Update successful.");
                } else {
                    accInv.undoCommand();
                }
            }
        });

        // Create panel for input fields and labels
        JPanel inputPanel = new JPanel(new GridLayout(3, 2));
        inputPanel.add(userNameLabel);
        inputPanel.add(userNameField);
        inputPanel.add(emailLabel);
        inputPanel.add(emailField);

        // Add components to frame
        getContentPane().add(inputPanel, BorderLayout.CENTER);
        getContentPane().add(updateButton, BorderLayout.SOUTH);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
            	LoginImpl loginImpl = LoginImpl.getInstance();
                // Check if the user is logged in
                int userId = loginImpl.getUserId();
                if (loginImpl.isLoggedIn(userId)) {
                    UpdateEmailNameGUI upEmNmPageGUI = new UpdateEmailNameGUI();
                    upEmNmPageGUI.setVisible(true);
                } else {
                    // Show login GUI or perform other actions as needed
                    LoginGUI loginGUI = new LoginGUI();
                    loginGUI.setVisible(true);
                }
            }
        });
    }
}
