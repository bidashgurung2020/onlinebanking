package GUI;

import javax.swing.*;
import Implementation.LoginImpl;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class UpdatePageGUI extends JFrame {
    public UpdatePageGUI() {
        setTitle("Update Page");
        setSize(300, 200);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);

        // Create buttons
        JButton updateEmailNameButton = new JButton("Update Email and Name");
        JButton updatePinButton = new JButton("Update PIN");

        // Add action listeners to buttons
        updateEmailNameButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
                UpdateEmailNameGUI updateEmailNameGUI = new UpdateEmailNameGUI();
                updateEmailNameGUI.setVisible(true);
            }
        });

        updatePinButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
                UpdatePinGUI updatePinGUI = new UpdatePinGUI();
                updatePinGUI.setVisible(true);
            }
        });

        // Create panel for buttons
        JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 10, 5));
        buttonPanel.add(updateEmailNameButton);
        buttonPanel.add(updatePinButton);

        // Add button panel to frame
        getContentPane().add(buttonPanel, BorderLayout.CENTER);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
            	LoginImpl loginImpl = LoginImpl.getInstance();
                // Check if the user is logged in
                int userId = loginImpl.getUserId();
                if (loginImpl.isLoggedIn(userId)) {
                    UpdatePageGUI upPageGUI = new UpdatePageGUI();
                    upPageGUI.setVisible(true);
                } else {
                    // Show login GUI or perform other actions as needed
                    LoginGUI loginGUI = new LoginGUI();
                    loginGUI.setVisible(true);
                }
            }
        });
    }
}
