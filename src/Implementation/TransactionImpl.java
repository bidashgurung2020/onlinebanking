package Implementation;


//import framework;
import com.OnlineBanking.Transaction.*;


import javax.swing.JOptionPane;

//Database 
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.ResultSet;


//TransactionStateSuccessfull Class
class TransactionStateSuccessfull implements TransactionState{
	private TransactionContext transact;
	
	public TransactionStateSuccessfull(TransactionContext transact) {
		this.transact= transact;
	}

	@Override
	public void transaction() {
		transact.setState(transact.getTransactionSuccessfullState());
		JOptionPane.showMessageDialog(null, "Transaction is Successfull", "Success", JOptionPane.INFORMATION_MESSAGE);
	}
	
}

//TransactionStateUnsuccessfull class
class TransactionStateUnsuccessfull implements TransactionState{
private TransactionContext transact;
	
	public TransactionStateUnsuccessfull(TransactionContext transactionContext) {
		this.transact= transactionContext;
	}

	@Override
	public void transaction() {
		transact.setState(transact.getTransactionUnsuccessfullState());
		JOptionPane.showMessageDialog(null, "Transaction is Unsuccessfull due to insufficient Balance", "Error", JOptionPane.ERROR_MESSAGE);
	}

	
}

//Context State Class
class TransactionContext{
	private TransactionState transactionSuccessfull;
	private TransactionState transactionUnsuccessfull;
	private TransactionState currentState;
	
	public TransactionContext() {
		transactionSuccessfull = new TransactionStateSuccessfull(this);
        transactionUnsuccessfull = new TransactionStateUnsuccessfull(this);
        currentState = transactionUnsuccessfull;
	}
	
	public void setState(TransactionState state) {
		this.currentState=state;
	}
	
	public TransactionState getCurrentState() {
		return currentState;
	}
	
	public TransactionState getTransactionSuccessfullState() {
		return transactionSuccessfull;
	}
	
	public TransactionState getTransactionUnsuccessfullState() {
		return transactionUnsuccessfull;
	}
	
}



//RechargeConcreteStratgeyClass
class Recharge extends TransactionStrategy{
	private TransactionContext transact;
	double balance;

    public Recharge(TransactionContext transact) {
        this.transact = transact;
    }
	
	@Override
	public void recharge(double amount, String fromAccount, int phoneNum, double balance) {
		this.balance = balance;
		if(balance<amount) {
			transact.getCurrentState().transaction();
		}else {
			balance-=amount;
			updateBalanceInDatabase(fromAccount, balance);
			transact.getTransactionSuccessfullState().transaction();
			JOptionPane.showMessageDialog(null, 
				    "Recharged Nu." + amount + "\n" + 
				    "To contact Number: " + phoneNum + "\n" + 
				    "Balance left: " + balance, 
				    "Success", 
				    JOptionPane.INFORMATION_MESSAGE);

		}
	}

	@Override
	public void transfer(double amount, String fromAccount, String toAccount, double balance) {
		// TODO Auto-generated method stub
		System.out.println();
		
	}
	
	 private void updateBalanceInDatabase(String accountId, double newBalance) {
	        try (Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/pattern_design", "root", "")) {
	            String query = "UPDATE users SET balance = ? WHERE accNo = ?";
	            try (PreparedStatement statement = connection.prepareStatement(query)) {
	                statement.setDouble(1, newBalance);
	                statement.setString(2, accountId);
	                statement.executeUpdate();
	            }
	        } catch (SQLException e) {
	            JOptionPane.showMessageDialog(null, "Error updating balance in database: " + e.getMessage(), "Database Error", JOptionPane.ERROR_MESSAGE);
	        }
	    }

}

//TransferConcreteStrategyClass
class Transfer extends TransactionStrategy{
	private TransactionContext transact;
	double balance;
	 public Transfer(TransactionContext transact) {
	        this.transact = transact;
	    }
	@Override
	public void transfer(double amount, String fromAccount, String toAccount, double balance) {
		this.balance = balance;
		
		// Check if fromAccount and toAccount are the same
        if (fromAccount.equals(toAccount)) {
            JOptionPane.showMessageDialog(null, "Source and destination accounts cannot be the same", "Error", JOptionPane.ERROR_MESSAGE);
            return;
        }

		 if (!accountExistsInDatabase(toAccount)) {
		        JOptionPane.showMessageDialog(null, "Destination account does not exist", "Error", JOptionPane.ERROR_MESSAGE);
		        return;
		 }
		if(balance<amount) {
			transact.getCurrentState().transaction();
		}else {
			balance-=amount;
			double toAccountBalance = getBalanceFromDatabase(toAccount);
			double newBalanceTo = toAccountBalance + amount;
			updateBalanceInDatabase(toAccount, newBalanceTo);
			
			updateBalanceInDatabase(fromAccount, balance);
			transact.getTransactionSuccessfullState().transaction();
			JOptionPane.showMessageDialog(null, 
				    "Transferred Nu." + amount + "\n" + 
				    "to Account Number: " + toAccount + "\n" + 
				    "Balance Left: " + balance, 
				    "Success", 
				    JOptionPane.INFORMATION_MESSAGE);
		}
	}
	 
	// To cross check whether the toAccount exist in database
	 private boolean accountExistsInDatabase(String accountId) {
	        try (Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/pattern_design", "root", "")) {
	            String query = "SELECT COUNT(*) FROM users WHERE accNo = ?";
	            try (PreparedStatement statement = connection.prepareStatement(query)) {
	                statement.setString(1, accountId);
	                try (ResultSet resultSet = statement.executeQuery()) {
	                    if (resultSet.next()) {
	                        return resultSet.getInt(1) > 0;
	                    }
	                }
	            }
	        } catch (SQLException e) {
	            JOptionPane.showMessageDialog(null, "Error checking account existence in database: " + e.getMessage(), "Database Error", JOptionPane.ERROR_MESSAGE);
	        }
	        return false;
	    }
	 
	@Override
	public void recharge(double amount, String fromAccount, int phoneNum, double balance) {
		// TODO Auto-generated method stub
		System.out.println();
	}
	private void updateBalanceInDatabase(String accountId, double newBalance) {
        try (Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/pattern_design", "root", "")) {
            String query = "UPDATE users SET balance = ? WHERE accNo = ?";
            try (PreparedStatement statement = connection.prepareStatement(query)) {
                statement.setDouble(1, newBalance);
                statement.setString(2, accountId);
                statement.executeUpdate();
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Error updating balance in database: " + e.getMessage(), "Database Error", JOptionPane.ERROR_MESSAGE);
        }      
      }
	   // Get balance of ToAccount user
    private double getBalanceFromDatabase(String accountId) {
        double balance = 0.0;
        try (Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/pattern_design", "root", "")) {
            String query = "SELECT COUNT(*), balance FROM users WHERE accNo = ?";
            try (PreparedStatement statement = connection.prepareStatement(query)) {
                statement.setString(1, accountId);
                try (ResultSet resultSet = statement.executeQuery()) {
                    if (resultSet.next()) {
                        if (resultSet.getInt(1) > 0) {
                            balance = resultSet.getDouble("balance");
                        }
                    }
                }
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Error checking account existence in database: " + e.getMessage(), "Database Error", JOptionPane.ERROR_MESSAGE);
        }
        return balance;
    }
}


// Context strategy class
public class TransactionImpl {
	private TransactionContext transact;
	public TransactionStrategy transactionStrategy;
	
	public TransactionImpl() {
        this.transact = new TransactionContext();
    }
	
	
	public void setTransactionStrategy(TransactionStrategy transactionStrategy) {
		this.transactionStrategy= transactionStrategy;
	}
	
	public void performRecharge(double amount, String fromAccount, int phoneNum, double balance) {
		setTransactionStrategy(new Recharge(transact));
        transactionStrategy.recharge(amount, fromAccount, phoneNum, balance);
    }

    public void performTransfer(double amount, String fromAccount, String toAccount, double balance) {
    	setTransactionStrategy(new Transfer(transact));
        transactionStrategy.transfer(amount, fromAccount, toAccount, balance);
    }
	
}
