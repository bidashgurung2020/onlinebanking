package Implementation;
import com.OnlineBanking.Messaging.AdminSubject;
import com.OnlineBanking.Messaging.UserObserver;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;
import javax.swing.table.DefaultTableModel;


class concreteAdminSubject implements AdminSubject{
	private static final String DB_URL = "jdbc:mysql://localhost:3306/pattern_design";
    private static final String DB_USER = "root";
    private static final String DB_PASSWORD = "";
    private LoginImpl login;

	@Override
	 public void sendMessage(String message) {
		login= LoginImpl.getInstance();
        int userId = login.getUserId();
        
        if (isAdmin(userId)) {
            addMessageToDatabase(message, userId);
        } else {
            throw new SecurityException("Only admins can send messages");
        }
    }
	 private boolean isAdmin(int userId) {
	        boolean isAdmin = false;
	        String query = "SELECT role FROM users WHERE id = ?";
	        
	        try (Connection connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
	             PreparedStatement statement = connection.prepareStatement(query)) {
	             
	            statement.setInt(1, userId);
	            try (ResultSet resultSet = statement.executeQuery()) {
	                if (resultSet.next()) {
	                    String role = resultSet.getString("role");
	                    isAdmin = "admin".equalsIgnoreCase(role);
	                }
	            }
	        } catch (SQLException e) {
	            e.printStackTrace();
	        }
	        
	        return isAdmin;
	    }

	    private void addMessageToDatabase(String message, int userId) {
	        String insertQuery = "INSERT INTO messages (message, created_by) VALUES (?, ?)";
	        
	        try (Connection connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
	             PreparedStatement statement = connection.prepareStatement(insertQuery)) {
	             
	            statement.setString(1, message);
	            statement.setInt(2, userId);
	            statement.executeUpdate();
	        } catch (SQLException e) {
	            e.printStackTrace();
	        }
	    }
}

class concreteUserObserver implements UserObserver{
	
	private static final String DB_URL = "jdbc:mysql://localhost:3306/pattern_design";
    private static final String DB_USER = "root";
    private static final String DB_PASSWORD = "";
    
    private DefaultTableModel tableModel;

    public concreteUserObserver(DefaultTableModel tableModel) {
        this.tableModel = tableModel;
    }


	@Override
	public void getMessage() {
		fetchMessagesFromDatabase();
	}
	
	private void fetchMessagesFromDatabase() {
        String query = "SELECT message, created_at FROM messages ORDER BY created_at DESC";

        try (Connection connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
             PreparedStatement statement = connection.prepareStatement(query);
             ResultSet resultSet = statement.executeQuery()) {

            // Clear the table model
            tableModel.setRowCount(0);

            // Populate the table model with data from the database
            while (resultSet.next()) {
                Vector<String> row = new Vector<>();
                row.add(resultSet.getString("message"));
                row.add(resultSet.getString("created_at"));
                tableModel.addRow(row);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}

public class AdminMessaging {
	private AdminSubject admin;
	private UserObserver user;
	public String message;
	public String option;
	public DefaultTableModel tableModel;
	
	public void executeAdminMessaging(String option) {
		if(option.equals("send")) {
			admin = new concreteAdminSubject();
			admin.sendMessage(message);
		}else if(option.equals("receive")) {
			user = new concreteUserObserver(tableModel);
			user.getMessage();
		}
	}

}
