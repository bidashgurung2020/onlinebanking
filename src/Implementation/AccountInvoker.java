package Implementation;
import com.OnlineBanking.Profile.*;

//Database 
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
//import java.sql.ResultSet;

import javax.swing.JOptionPane;

//Concrete Command class
class updateEmailName implements AccountCommand{
private AccountManager accManage;
private int userId;
private String email;
private String userName;

public updateEmailName(AccountManager accManage, int userId, String email, String userName) {
    this.accManage = accManage;
    this.userId = userId;
    this.email = email;
    this.userName = userName;
}

	@Override
	public void execute() {
		accManage.updateEmNm(userId, email, userName);
		
	}

	@Override
	public void undo() {
		 JOptionPane.showMessageDialog(null, "Updating Email and Name is canceled.", "Update Status", JOptionPane.INFORMATION_MESSAGE);
		
	}
	
}

// Concrete Command class
class updatePin implements AccountCommand{
	private AccountManager accManage;
	private int userId;
	private int mPin;
	private int tPin;
	
	 public updatePin(AccountManager accManage, int userId, int mPin, int tPin) {
	        this.accManage = accManage;
	        this.userId = userId;
	        this.mPin = mPin;
	        this.tPin = tPin;
	    }

	@Override
	public void execute() {
		accManage.updatePin(userId, mPin, tPin);
		
	}

	@Override
	public void undo() {
		 JOptionPane.showMessageDialog(null, "Updating tPin and mPin is canceled.", "Update Status", JOptionPane.INFORMATION_MESSAGE);
		
	}
	
}

// Receiver class
class AccountManager{
	 public void updateEmNm(int userId, String email, String userName) {
	        try (Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/pattern_design", "root", "")) {
	            String query = "UPDATE users SET email = ?, userName = ? WHERE id = ?";
	            try (PreparedStatement statement = connection.prepareStatement(query)) {
	                statement.setString(1, email);
	                statement.setString(2, userName);
	                statement.setInt(3, userId);
	                statement.executeUpdate();
	            }
	        } catch (SQLException e) {
	            JOptionPane.showMessageDialog(null, "Error updating email and name in database: " + e.getMessage(), "Database Error", JOptionPane.ERROR_MESSAGE);
	        }
	    }
	
	 public void updatePin(int userId, int mPin, int tPin) {
	        try (Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/pattern_design", "root", "")) {
	            String query = "UPDATE users SET mPin = ?, tPin = ? WHERE id = ?";
	            try (PreparedStatement statement = connection.prepareStatement(query)) {
	                statement.setInt(1, mPin);
	                statement.setInt(2, tPin);
	                statement.setInt(3, userId);
	                statement.executeUpdate();
	            }
	        } catch (SQLException e) {
	            JOptionPane.showMessageDialog(null, "Error updating PINs in database: " + e.getMessage(), "Database Error", JOptionPane.ERROR_MESSAGE);
	        }
	    }
}

// Invoker class
public class AccountInvoker {
	private AccountCommand command;
	private AccountManager accManage;
	public int userId;
	public String email;
	public String userName;
	public int mPin;
	public int tPin;

    public void setCommand(String option) {
    	accManage = new AccountManager();
        if (option.equals("Email&Name")) {
        	this.command = new updateEmailName(accManage, userId, email, userName);
        }else if (option.equals("pin")){
        	this.command = new updatePin(accManage, userId, mPin, tPin);
        }
    }

    public void executeCommand() {
        if (command != null) {
            command.execute();
        }
    }

    public void undoCommand() {
        if (command != null) {
            command.undo();
        }
    }
}
