package Implementation;
import javax.swing.JOptionPane;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

//Import framework
import com.OnlineBanking.auth.*;
import com.OnlineBanking.util.*;

// Database 
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;


//Concrete userName handler
class userNameHandler implements ValidationHandler {
 private ValidationHandler nextHandler;

 @Override
 public void setNextHandler(ValidationHandler handler) {
     this.nextHandler = handler;
 }

 @Override
 public void handleValidationRequest(User user) {
     String userName = user.getUserName();

     try {
         // Validate username to be text and not contain any mixed characters
         String userNamePattern = "^[a-zA-Z]+$";
         if (!userName.matches(userNamePattern)) {
             throw new AppError("Username should only contain alphabetic characters and no mixed characters.", 400);
         }

         // Proceed to next handler if available
         if (nextHandler != null) {
             nextHandler.handleValidationRequest(user);
         }

     } catch (AppError e) {
         throw e;
     }
 }
}


//Concrete cid handler
class cidHanlder implements ValidationHandler{
	private ValidationHandler nextHandler;

	@Override
	public void setNextHandler(ValidationHandler handler) {
		this.nextHandler=handler;
	}

	@Override
	public void handleValidationRequest(User user) {
		try {
			String cid = user.getcId();
			int length = cid.length();

	        if (length!= 11) {
	            throw new AppError("CID length should be 11 digits.", 400);

	        }

	        if (nextHandler != null) {
	            nextHandler.handleValidationRequest(user);
	        }
	   
	    } catch (AppError e) {
	        throw e;
	        
	    }
	}	
}


//Concrete email handler
class emailHandler implements ValidationHandler{
	private ValidationHandler nextHandler;

	@Override
	public void setNextHandler(ValidationHandler handler) {
		this.nextHandler=handler;
	}

	@Override
	public void handleValidationRequest(User user) {
		String email = user.getEmail();
		 try {
        String emailPattern = "^[a-zA-Z0-9_+&*-]+(?:\\.[a-zA-Z0-9_+&*-]+)*@gmail\\.com$";;
        Pattern pattern = Pattern.compile(emailPattern);
        Matcher matcher = pattern.matcher(email);

        // Check if the email matches the pattern
        if (!matcher.matches()) {
            throw new AppError("Email format is incorrect.", 400);
        }

        if (nextHandler != null) {
            nextHandler.handleValidationRequest(user);
        }

    } catch (AppError e) {
        throw e;
    }
		
	}
}

//Concrete accountNo handler
class accNoHandler implements ValidationHandler{
 private ValidationHandler nextHandler;
 
 @Override
	public void setNextHandler(ValidationHandler handler) {
		this.nextHandler=handler;
	}

	@Override
	public void handleValidationRequest(User user) {
		String accNo = user.getAccNo();
		int length = accNo.length();
		try {
	        if (length!= 10) {
	            throw new AppError("Account Number length should be 10 digits.", 400);
	        }

	        if (nextHandler != null) {
	            nextHandler.handleValidationRequest(user);
	        }
	   
	    } catch (AppError e) {
	        throw e;
	    }
		
	}
	
}


//Concrete pin handler
class pinHandler implements ValidationHandler{
	private ValidationHandler nextHandler;

	@Override
	public void setNextHandler(ValidationHandler handler) {
		 throw new UnsupportedOperationException("pinHandler should be the last handler.");
	}

	@Override
	public void handleValidationRequest(User user) {
		int mPin = user.getmPin();
		int tPin = user.gettPin();
		
		int mPinLen = String.valueOf(mPin).length();
		int tPinLen = String.valueOf(tPin).length();
		
		try {
	        if (mPinLen !=4 || tPinLen!=4) {
	            throw new AppError("M-Pin and T-Pin should be length of 4 ", 400);
	        }

	        if (nextHandler != null) {
	            nextHandler.handleValidationRequest(user);
	        }
	   
	    } catch (AppError e) {
	        throw e;
	    }
		
	}
}

// Realsubject class
class SignUpRealSubject implements SignUpProxy{
	
	 private static final String DB_URL = "jdbc:mysql://localhost:3306/pattern_design";
	 private static final String DB_USERNAME = "root";
	 private static final String DB_PASSWORD = "";

	@Override
	public void signUp(User user) {
	    try (Connection connection = DriverManager.getConnection(DB_URL, DB_USERNAME, DB_PASSWORD)) {
	        // Prepare the SQL statement
	        String sql = "INSERT INTO users (userName, cId, accNo, email, mPin, tPin) VALUES (?, ?, ?, ?, ?, ?)";
	        try (PreparedStatement statement = connection.prepareStatement(sql)) {
	            // Set parameters for the prepared statement
	            statement.setString(1, user.getUserName());
	            statement.setString(2, user.getcId());
	            statement.setString(3, user.getAccNo());
	            statement.setString(4, user.getEmail());
	            statement.setInt(5, user.getmPin());
	            statement.setInt(6, user.gettPin());
	            
	            // Execute the insert statement
	            int rowsInserted = statement.executeUpdate();
	            if (rowsInserted > 0) {
	                JOptionPane.showMessageDialog(null, "User signed up successfully.", "Success", JOptionPane.INFORMATION_MESSAGE);
	            } else {
	                JOptionPane.showMessageDialog(null, "Failed to sign up user.", "Error", JOptionPane.ERROR_MESSAGE);
	            }
	        }
	    } catch (SQLException e) {
	        JOptionPane.showMessageDialog(null, "Error during sign-up: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
	    }
	}
	
}

// Proxy class
public class SignUpProxyImpl implements SignUpProxy {
	 private final SignUpRealSubject realSubject;
	 private final ValidationHandler validationHandler;
	 
	 public SignUpProxyImpl() {
		 	
	        this.realSubject = new SignUpRealSubject();
	        
	        this.validationHandler = new userNameHandler();
	        
	        ValidationHandler cid = new cidHanlder();
	        ValidationHandler email = new emailHandler();
	        ValidationHandler acc = new accNoHandler();
	        ValidationHandler pin = new pinHandler();
	        
	        this.validationHandler.setNextHandler(cid);
	        cid.setNextHandler(email);
	        email.setNextHandler(acc);
	        acc.setNextHandler(pin);
	    }
	@Override
	public void signUp(User user) {
		try {
	        validationHandler.handleValidationRequest(user);
	        realSubject.signUp(user);
	    } catch (AppError e) {
	        // Handle validation errors or other errors
	        String errorMessage = "Error during sign-up: " + e.getMessage();
	        // Display the error message using JOptionPane
	        JOptionPane.showMessageDialog(null, errorMessage, "Error", JOptionPane.ERROR_MESSAGE);
	        
	        throw e;
	    }
		
	}
   
}

