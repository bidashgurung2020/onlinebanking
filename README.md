## Folder Structure

The workspace contains two folders by default, where:

- `src`: It contain the Implementaion, GUI of Online Banking system and UML folder which contain class diagram.
- `lib`: It contain Java-sql-connector.jar to connect to database and OnlineBanking.jar file Which is our defined framework.


Role:

Deki Lhazom - Project Lead

Ashish Nepal - Project Manager

Dal Bahadur Rai - Backend Developer

Bidash Gurung - Backend Developer


